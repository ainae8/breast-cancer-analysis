# **Breast Cancer Analysis** 

## Introduction 

In 2020, 2.2 million women around the world was diagnosed with breast cancer with 30% of the afficted women having succumbed to the disease. 

## Data Assessment 

### Importing the Dataset

The data set which is stored within a comma seperated value (csv) file was imported into a Jupyter notebook running on a Python 3 kernel using the Panda's library. 
The first five rows of the dataset are shown in **Figure 1**.

![Figure 1](Figure_file/Figure_1.png) 

From the first five row we can identify our target variable for the machine learning model being the column *diagnosis* which classifies the cell as B for benign and M for malignant. 

### Size of Dataset 

![Figure 2](Figure_file/Figure_2.png)

The dimansion of the dataset is shown in **Figure 2**. The dataset relatively small.

### General Information

![Figure 3](Figure_file/Figure_3.png)

Using the Pandas's info method we can obtain the basic information regarding the dataset. All the attributes within the dataset are correctly typed and none of the attributes contains *null* values.

### Descriptive Statistics

![Figure 4](Figure_file/Figure_4.png)

The Panda's describe method shows the descriptive statistic of the feature and it can be noted that 2 of the features (i.e. *concavity_mean* and *concave points _mean*) has zero values. 

## Data Cleaning 

In section 3.4 of this paper, it was noted that while the dataset does not contain any null values. Thus the only process that is required will be the dropping of the unique identifier column *id* and the removal of outliers within the dataset.

### Removing Redundant Features

![Figure 5](Figure_file/Figure_5.png)

**Figure 5** shows the column *id* being removed from the dataset using the Panda's drop method. 

### Removing Outliers

There are several methods for addressing outliers within a dataset, such as;

- Z-core - which measures the distance between the point and the mean
- Inter Quartile Range (IQR) - data points beyond the range boundary
- DBScan - density based clustering method that detects out of group anomalies

The outlier removal method that will be used for this paper will be the z-score method it the one that can be easiest to implement within Python.

![Figure 6](Figure_file/Figure_6.png)

As seen on **Figure 6**, the Z-score outlier removal technique was able to remove 31 cloud points from the dataset. 

Alternatively, we can view the number of outliers from each independent variable using boxplot, as shown in **Figure 7**.

![Figure 7](Figure_file/Figure_7.png)

The outliers are denoted within the boxplot as diamond shaped points which are calculated based on the IQR. The formula for this calculation is shown in **Figure 8**.

## Measuring Balance Of The Dataset 

**Figure 9** shows a pie chart which shows that the benign class makes up *64%* of the majority of the dataset.

![Figure 8](Figure_file/Figure_8.png)

![Figure 9](Figure_file/piechart.png)

## Exploratory Data Analysis

### Distribution Plot

![Figure 10](Figure_file/histogram.png)

From the distribution plot of the five features shown in **Figure 10**, it can be observed that the size of the malignant tumour cells is substantially larger than its benign counterpart. 

### Feature Comparison

**Figure 11** shows a violin plot that confirms the observations from the distribution plot, that the malignant cells are generally larger than the benign cells. 

![Figure 11](Figure_file/violin.png)

### Feature Selection

In order to create an efficient and accurate machine learning model, proper feature selection is required. The heatmap in **FIgure 16** shows the Pearson correlation matrix of the independent variable. 

![Figure 12](Figure_file/correlationmatrix.png)

The final list of features consists of the following variables;

- Texture Mean
- Area Mean
- Compactness Mean
- Concavity Mean
- Concave Points Mean





